package com.whitepages
package api

trait WhitepagesService extends App {
  import spray.json._
  import DefaultJsonProtocol._

  val host = "https://proapi.whitepages.com/2.0/"
  // val host = "http://localhost:8080/"

  type Name = String
  type PhoneNumber = String
  type City = String
  type State = String
  type Zip = String
  type Address = String

  case class ApiKey(key: String)

  sealed trait BaseEntity {
    def id: ID

    /**
     * Preference score among entities(for sorting), default: durable preferred against ephemeral
     */
    def score: Double = if (id.durable) 1 else 0
  }

  sealed trait Entity extends BaseEntity

  sealed trait Relation extends BaseEntity {
    def valid_for: Option[Period]

    def recent: Boolean = valid_for match {
      case None => true // null means 'recent'!?
      case Some(period) if !period.start.isEmpty && period.stop.isEmpty => true
      case _ => false
    }

    override def score: Double = super.score + (if (recent) 1 else 0)
  }

  sealed trait HistoricalRelation extends Relation {
    def is_historical: Option[Boolean]

    def isHistorical: Boolean = is_historical.getOrElse(false)

    override def score: Double = super.score + (if (isHistorical) 0 else 1)

  }

  trait RelatedLocations {
    def locations: List[RelatedLocation]

    /**
     * Valid and not historic locations are preferred. The first one is choosed.
     */
    def relatedLocations(implicit dictionary: Dictionary): List[Option[Location]] =
      locations.filter(location => location.recent && !location.isHistorical)
        .map(relatedLocation => dictionary.location(relatedLocation.id.key))

  }

  trait BestName {
    def bestName: Option[String]
  }

  trait RelatedEntities {

    def related: List[Either[RelatedPerson, RelatedBusiness]]

    def eitherValid(e: Either[RelatedPerson, RelatedBusiness]): Boolean = e match {
      case Left(person) => person.recent
      case Right(business) => business.recent
    }

    /**
     * Returns valid entities.
     */
    def relatedEntities(implicit dictionary: Dictionary): List[Option[Either[Person, Business]]] = {
      related.filter(eitherValid).map(
        _.left.map(p => dictionary.person(p.id.key).get).right.map(b => dictionary.business(b.id.key).get)
      ).map(e => Some(e))
    }

  }

  trait RelatedPhones {
    def phones: List[RelatedPhone]

    /**
     * Not valid and historic phone numbers are filtered.
     */
    def relatedPhones(implicit dictionary: Dictionary): List[Option[Phone]] = {
      phones.filter(phone => phone.recent && !phone.isHistorical)
        .map(relatedPhone => dictionary.phone(relatedPhone.id.key))
    }

  }

  trait ID {
    def key: String
    def url: Option[String]
    def typ: String
    def uuid: String
    def durability: String
    def durable: Boolean = durability == "Durable"
  }

  case class PhoneID(key: String, url: Option[String], uuid: String, durability: String) extends ID {
    def typ = "Phone"
  }

  case class PersonID(key: String, url: Option[String], uuid: String, durability: String) extends ID {
    def typ = "Person"
  }

  case class BusinessID(key: String, url: Option[String], uuid: String, durability: String) extends ID {
    def typ = "Business"
  }

  case class LocationID(key: String, url: Option[String], uuid: String, durability: String) extends ID {
    def typ = "Location"
  }

  case class Date(year: Option[Int], month: Option[Int], day: Option[Int])

  case class Period(start: Option[Date], stop: Option[Date])

  case class RelatedPerson(id: PersonID, valid_for: Option[Period]) extends Relation

  case class RelatedBusiness(id: BusinessID, valid_for: Option[Period]) extends Relation

  case class RelatedPhone(id: PhoneID, valid_for: Option[Period], is_historical: Option[Boolean],
    contact_type: Option[String]) extends Relation with HistoricalRelation

  case class RelatedLocation(id: LocationID, valid_for: Option[Period], is_historical: Option[Boolean], contact_type: Option[String]) extends Relation with HistoricalRelation

  case class PersonName(salutation: Option[String], first_name: Option[String],
    middle_name: Option[String], last_name: Option[String], suffix: Option[String],
    valid_for: Option[Period])

  case class Person(id: PersonID, typ: String, gender: Option[String], names: List[PersonName], locations: List[RelatedLocation], phones: List[RelatedPhone], best_name: Option[String], best_location: Option[BestLocation]) extends Entity with RelatedPhones with RelatedLocations with BestName {

    def bestName = best_name
  }

  case class Business(id: BusinessID, name: Option[String], locations: List[RelatedLocation],
      phones: List[RelatedPhone]) extends Entity with RelatedPhones with RelatedLocations with BestName {

    def bestName = name

  }

  case class PhoneReputation(spam_score: Double)

  case class Phone(id: PhoneID, line_type: Option[String], belongs_to: Option[List[Either[RelatedPerson, RelatedBusiness]]], associated_locations: Option[List[RelatedLocation]], is_valid: Option[Boolean], phone_number: String, country_calling_code: String, extension: Option[String], reputation: Option[PhoneReputation], best_location: Option[BestLocation]) extends Entity with RelatedEntities with RelatedLocations {

    def locations = associated_locations.getOrElse(List())

    def related = belongs_to.getOrElse(List())

    def isLandline: Boolean = line_type == "Landline"

    def isValid: Boolean = is_valid.getOrElse(false)

    def reputationScore: Double = reputation.map(_.spam_score).getOrElse(0)

    // valid and landline phone numbers are preferred, reputation is considered as well
    override def score: Double = {
      super.score + reputationScore +
        (if (isValid) 1 else 0) +
        (if (isLandline) 1 else 0)
    }

  }

  case class BestLocation(id: LocationID)

  // I cut this short intentionally, the limit of 22 is nice :)
  case class Location(id: LocationID, typ: String, legal_entities_at: Option[List[Either[RelatedPerson, RelatedBusiness]]],
      city: Option[String], postal_code: Option[String], zip4: Option[String],
      state_code: Option[String], address: Option[String],
      street_name: Option[String], street_type: Option[String],
      standard_address_line1: Option[String], standard_address_location: Option[String]) extends Entity with RelatedEntities {

    def related = legal_entities_at.getOrElse(List())

  }

  case class ErrorResult(error: Error)

  case class Error(name: String, message: String)

  case class Result(results: List[String], dictionary: Map[String, Entity])

  trait IdJsonWriteFormat[T <: ID] {
    def write(id: T) = id.url match {
      case Some(url) => JsArray(JsString(id.key), JsString(url), JsString(id.typ), JsString(id.uuid), JsString(id.durability))
      case None => JsArray(JsString(id.key), JsString(id.typ), JsString(id.uuid), JsString(id.durability))
    }
  }

  trait BaseIdJsonReadFormat[T <: ID] {

    def fields(value: JsValue): Seq[JsValue] =
      value.asJsObject.getFields("key", "url", "typ", "uuid", "durability")

    def error(): PartialFunction[Seq[JsValue], T] = {
      case fs => throw new DeserializationException(idType + "ID expected" + fs.mkString(" | "))
    }

    def idType(): String
  }

  trait IdJsonReadFormat[T <: ID] extends BaseIdJsonReadFormat[T] {

    def id(): PartialFunction[Seq[JsValue], T] = {
      case Seq(JsString(key), JsString(url), JsString(id_type), JsString(uuid), JsString(durability)) if idType == id_type => ctor(key, Some(url), uuid, durability)
      case Seq(JsString(key), JsString(id_type), JsString(uuid), JsString(durability)) if idType == id_type => ctor(key, None, uuid, durability) // missing url field
      case Seq(JsString(key), JsNull, JsString(id_type), JsString(uuid), JsString(durability)) if idType == id_type => ctor(key, None, uuid, durability) // null url field
    }

    def read(value: JsValue): T = id.orElse(error)(fields(value))

    def ctor(key: String, url: Option[String], uuid: String, durability: String): T

  }

  implicit object IdJsonFormat extends RootJsonFormat[ID] with IdJsonWriteFormat[ID] with BaseIdJsonReadFormat[ID] {

    def read(value: JsValue): ID = {
      val fields: Seq[JsValue] =
        value.asJsObject.getFields("key", "url", "typ", "uuid", "durability")
      PhoneIdJsonFormat.id.orElse(LocationIdJsonFormat.id).orElse(PersonIdJsonFormat.id)
        .orElse(BusinessIdJsonFormat.id).orElse(error)(fields)
    }

    def idType = ""

  }

  implicit object PhoneIdJsonFormat extends RootJsonFormat[PhoneID] with IdJsonWriteFormat[PhoneID] with IdJsonReadFormat[PhoneID] {

    def idType = "Phone"

    def ctor(key: String, url: Option[String], uuid: String, durability: String): PhoneID =
      PhoneID(key, url, uuid, durability)

  }
  implicit object LocationIdJsonFormat extends RootJsonFormat[LocationID] with IdJsonWriteFormat[LocationID] with IdJsonReadFormat[LocationID] {

    def idType = "Location"

    def ctor(key: String, url: Option[String], uuid: String, durability: String): LocationID =
      LocationID(key, url, uuid, durability)

  }
  implicit object PersonIdJsonFormat extends RootJsonFormat[PersonID] with IdJsonWriteFormat[PersonID] with IdJsonReadFormat[PersonID] {

    def idType = "Person"

    def ctor(key: String, url: Option[String], uuid: String, durability: String): PersonID =
      PersonID(key, url, uuid, durability)

  }
  implicit object BusinessIdJsonFormat extends RootJsonFormat[BusinessID] with IdJsonWriteFormat[BusinessID] with IdJsonReadFormat[BusinessID] {

    def idType = "Business"

    def ctor(key: String, url: Option[String], uuid: String, durability: String): BusinessID =
      BusinessID(key, url, uuid, durability)
  }

  implicit object EntityJsonFormat extends RootJsonFormat[Entity] {
    def write(p: Entity) = p match {
      case phone: Phone => phone.toJson
      case location: Location => location.toJson
      case person: Person => person.toJson
      case business: Business => business.toJson
    }

    def read(value: JsValue): Entity = {
      val id = value.asJsObject.getFields("id")(0).convertTo[ID]

      id match { // id type tells how to deserialize the json string
        case business: BusinessID => value.convertTo[Business]
        case location: LocationID => value.convertTo[Location]
        case person: PersonID => value.convertTo[Person]
        case phone: PhoneID => value.convertTo[Phone]
        case t => throw new IllegalStateException("unknown ID type: " + t)
      }
    }
  }

  implicit val bestLocationFormat = jsonFormat1(BestLocation)
  implicit val dateFormat = jsonFormat3(Date)
  implicit val periodFormat = jsonFormat2(Period)
  implicit val relatedPersonFormat = jsonFormat2(RelatedPerson)
  implicit val relatedBusinessFormat = jsonFormat2(RelatedBusiness)
  implicit val relatedPhoneFormat = jsonFormat4(RelatedPhone)
  implicit val relatedLocationFormat = jsonFormat4(RelatedLocation)
  implicit val personNameFormat = jsonFormat6(PersonName)
  implicit val personFormat = jsonFormat8(Person)
  implicit val businessFormat = jsonFormat4(Business)
  implicit val phoneReputationFormat = jsonFormat1(PhoneReputation)
  implicit val locationFormat = jsonFormat12(Location)
  implicit val phoneFormat = jsonFormat10(Phone)
  implicit val resultFormat = jsonFormat2(Result)
  implicit val errorFormat = jsonFormat2(Error)
  implicit val errorResultFormat = jsonFormat1(ErrorResult)

  trait Query {

    def +(key: String, value: String): Query

    def +(key: String, optValue: Option[String]): Query

    def url: String

  }

  object Query {

    def phone(phoneNumber: PhoneNumber)(implicit apiKey: ApiKey): Query =
      new QueryImpl("phone.json", apiKey) + ("phone_number", phoneNumber)

    def business(name: Name, city: Option[City] = None, state: Option[State] = None,
      zip: Option[Zip] = None, address: Option[Address] = None)(implicit apiKey: ApiKey): Query =
      (city, state, zip, address) match {
        case (None, None, None, None) =>
          throw new IllegalArgumentException("name and at least one parameter other parameter should be defined")
        case _ =>
          new QueryImpl("business.json", apiKey) + ("name", name) + ("city", city) +
            ("state_code", state) + ("postal_code", zip) + ("street_line_1", address)
      }

    def person(name: Name, city: Option[City] = None, state: Option[State] = None,
      zip: Option[Zip] = None, address: Option[Address] = None)(implicit apiKey: ApiKey): Query =
      new QueryImpl("person.json", apiKey) + ("name", name) + ("city", city) +
        ("state_code", state) + ("postal_code", zip) + ("street_line_1", address)

    def location(address: Address, zip: Option[Zip] = None,
      city: Option[City] = None, state: Option[State])(implicit apiKey: ApiKey): Query = {
      (zip, city) match {
        case (None, None) =>
          throw new IllegalArgumentException("zip or city should be defined")
        case _ =>
          new QueryImpl("location.json", apiKey) + ("city", city) + ("state_code", state) +
            ("postal_code", zip) + ("street_line_1", address)
      }
    }

    private class QueryImpl(query: String, service: String) extends Query {

      override def toString(): String = query

      def url(): String = host + service + "?" + query

      def this(service: String, apiKey: ApiKey) = this("api_key=" + apiKey.key, service)

      def +(key: String, value: String): Query =
        new QueryImpl(query + "&" + key + "=" + java.net.URLEncoder.encode(value, "UTF8"), service)

      def +(key: String, optValue: Option[String]): Query = optValue match {
        case Some(value) => this + (key, value)
        case None => this
      }
    }

  }

  case class Dictionary(dictionary: Map[String, Entity]) {

    def phone(id: String): Option[Phone] = dictionary.get(id).flatMap {
      case phone: Phone => Some(phone)
      case _ => None
    }

    def location(id: String): Option[Location] = dictionary.get(id).flatMap {
      case location: Location => Some(location)
      case _ => None
    }

    def person(id: String): Option[Person] = dictionary.get(id).flatMap {
      case person: Person => Some(person)
      case _ => None
    }

    def business(id: String): Option[Business] = dictionary.get(id).flatMap {
      case business: Business => Some(business)
      case _ => None
    }

  }

  // because Scala doesn't allow 'type' as a field name
  def repair(json: String): String = json.replaceAll("\"type\"", "\"typ\"")

  class WhitepagesException(msg: String) extends RuntimeException(msg)

}