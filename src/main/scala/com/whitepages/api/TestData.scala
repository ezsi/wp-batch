package com.whitepages
package api

import spray.json._

object TestData extends App with WhitepagesService {

  def load(filename: String): String = {
    val file: java.net.URL = getClass.getResource(filename)
    scala.io.Source.fromURL(file).mkString
  }

  def phone = load("/phone.json")

  def person = load("/person2.json")

  def business = load("/business.json")

  def location = load("/location.json")

  val json =

    """
	{
     "key":"Location.fd8d9ee3-50cf-4523-b5b4-3516d67174b5.Durable",
     "url":"https://proapi.whitepages.com/2.0/entity/Location.fd8d9ee3-50cf-4523-b5b4-3516d67174b5.Durable.json?api_key=KEYVAL",
     "type":"Location",
     "uuid":"fd8d9ee3-50cf-4523-b5b4-3516d67174b5",
     "durability":"Durable"
    }

	"""

  val jsonStr = repair(person)
  println(jsonStr)
  val obj = jsonStr.parseJson.convertTo[Result]

  println(obj)
}