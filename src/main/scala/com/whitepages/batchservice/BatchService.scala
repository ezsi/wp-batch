package com.whitepages.batchservice

import scala.util.{ Success, Failure }
import scala.concurrent.Future
import akka.actor._
import akka.event.Logging
import spray.routing._
import spray.http._
import spray.http.MediaTypes._
import spray.client.pipelining._
import spray.json._
import DefaultJsonProtocol._

import com.whitepages.api._

import spray.http.HttpHeaders.RawHeader

/**
 * ISSUES with API documentation
 * 1, Calling Phone data structure as Phone ID is misleading for me. It is not an ID but contains an ID.
 *
 * 2, Phone entity is_valid field according to the documentation is invalid if the is_valid is true!? :)
 *
 * 3, Documentation claims that the ID.url field is required but it is really not. I got return null for Ephemeral data.
 *
 * 4, Phone.best_location is not an ID (as documentation mentions) but a structure which contains a single 'id' field which itself is an ID for sure.
 *
 * 5, Phone.reputation is said to be an Integer, but it is a Phone Reputation data structure.
 * Beside that the spam_score is uninterpretable, according to the documentation it is the
 * likelihood of being a spam, but one of the example response for phone query returns 1 for
 * whitepages.com so it is probably not the spam score, but really the reputation of the phone
 * where 1 means that is a valid (not spammer) phone number.
 *
 * 6, On Reverse Location/Address page the example result is a person result not a location.
 *
 * Beside that the query parameter list contains "state_code" key instead of "state"!!!
 *
 * Beside that the query parameter list contains "name" key, if I add it I get back
 *   "Invalid input for shape ProReverseAddress: what must not be provided"
 *
 * Beside that :)  documentation states: "Note: You will need to give at least the street parameter, the state or zip, and an API key for a Reverse Location request."
 * but instead city or zip is required not state or zip!!!
 *
 *
 * 7, I generally could not find documentation about error responses!
 *
 *
 */

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class BathcServiceActor extends Actor with BatchService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)
}

// this trait defines our service behavior independently from the service actor
trait BatchService extends HttpService with WhitepagesService {

  val CSV_SEPARATOR = "|" // take it as a default

  case class DataResult(entity: Option[Either[Person, Business]], phone: Option[Phone], location: Option[Location], data: Data)

  object DataResult {
    def apply(entity: Option[Either[Person, Business]], phone: Option[Phone], location: Option[Location]): DataResult = {
      val name = entity.flatMap {
        case Left(person) => person.best_name
        case Right(business) => business.name
      }

      val (city, state, zip, address) = location.map(l =>
        (l.city, l.state_code, l.postal_code, l.address)).getOrElse((None, None, None, None))
      val data = Data(name, phone.map(_.phone_number), city, state, zip, address)
      DataResult(entity, phone, location, data)
    }
  }

  case class Data(name: Option[Name], phoneNumber: Option[PhoneNumber], city: Option[City],
      state: Option[State], zip: Option[Zip], address: Option[Address]) {

    def asString(mapper: Map[Int, Int]): String = {
      val v = Array(name.getOrElse(""), phoneNumber.getOrElse(""), city.getOrElse(""),
        state.getOrElse(""), zip.getOrElse(""), address.getOrElse(""))

      Array(v(mapper(0)), v(mapper(1)), v(mapper(2)), v(mapper(3)),
        v(mapper(4)), v(mapper(5))).mkString(CSV_SEPARATOR)
    }

    // count of non-empty elements
    def score(): Int = {
      def count(opt: Option[String]) = if (opt.isEmpty) 0 else 1

      count(name) + count(phoneNumber) + count(city) + count(state) + count(zip) + count(address)
    }

  }

  object Data {
    def apply(strings: Array[String], mapper: Map[Int, Int]): Data = {
      val m = (idx: Int) => mapper.get(idx).flatMap(idx => opt(strings(idx)))
      Data(m(0), m(1), m(2), m(3), m(4), m(5))
    }

    def opt(str: String): Option[String] = if (str.isEmpty) None else Some(str)

    def phonesToDatas(phones: List[Phone])(implicit dictionary: Dictionary): List[DataResult] = {
      for {
        phone <- phones
        entity <- phone.relatedEntities :+ None
        location <- phone.relatedLocations :+ None
      } yield DataResult(entity, Some(phone), location)
    }

    def phones(entity: Either[Person, Business])(implicit dictionary: Dictionary): List[Option[Phone]] = {
      entity.fold[List[Option[Phone]]]((p: Person) => p.relatedPhones, (b: Business) => b.relatedPhones)
    }

    def locations(entity: Either[Person, Business])(implicit dictionary: Dictionary): List[Option[Location]] = {
      entity.fold[List[Option[Location]]]((p: Person) => p.relatedLocations, (b: Business) => b.relatedLocations)
    }

    def entitiesToDatas(entities: List[Either[Person, Business]])(implicit dictionary: Dictionary): List[DataResult] = {

      for {
        entity <- entities
        phone <- phones(entity) :+ None
        location <- locations(entity) :+ None
      } yield DataResult(Some(entity), phone, location)
    }

    def locationsToDatas(locations: List[Location])(implicit dictionary: Dictionary): List[DataResult] = {

      for {
        location <- locations
        entity <- location.relatedEntities :+ None
        phone <- entity.map(e => phones(e) :+ None).getOrElse(List(None))
      } yield DataResult(entity, phone, Some(location))
    }

  }

  // using the enclosing ActorContext's or ActorSystem's dispatcher
  implicit def executionContext = actorRefFactory.dispatcher

  val system = ActorSystem("mySystem")
  val log = Logging(system, getClass)

  val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

  def request(query: Query): Future[Either[ErrorResult, Result]] = {
    println("request: " + query.url)
    val futResponse: Future[HttpResponse] = pipeline(Get(query.url))
    futResponse.map(response => {
      val json = response.entity.asString // response json
      repair(json).parseJson.convertTo[Either[ErrorResult, Result]]
    })
  }

  /**
   * Selects the best fill result or returns the original.
   */
  def selectBest(results: List[DataResult], original: Data): Data = {
    def acceptor(dataResult: DataResult): Boolean = {

      def match0(val1: Option[String], val2: Option[String]) = (val1, val2) match {
        case (Some(v1), Some(v2)) if v1 == v2 => true // same as original, its ok
        case (Some(v1), _) => false // not matching data should be discarded
        case (None, _) => true // original was none then accept new value
      }
      val data = dataResult.data

      match0(original.name, data.name) && match0(original.phoneNumber, data.phoneNumber) &&
        match0(original.city, data.city) && match0(original.state, data.state) &&
        match0(original.zip, data.zip) && match0(original.address, data.address)
    }

    def score(entity: Either[Person, Business]): Double =
      entity.fold[Double](p => p.score, b => b.score)

    results.filter(acceptor).sortBy {
      dataResult =>
        val phoneScore: Double = dataResult.phone.map(_.score).getOrElse(-1)
        val locationScore: Double = dataResult.location.map(_.score).getOrElse(-1)
        val entityScore: Double = dataResult.entity.map(score(_)).getOrElse(-1)
        val dataScore: Double = dataResult.data.score()
        phoneScore -> locationScore -> entityScore -> dataScore // ultimate sort solution :)
    }.reverse.headOption.map(_.data).getOrElse(original)
  }

  // returns fixed data 
  def fix(data: Data)(implicit apiKey: ApiKey): Future[Data] = {
    println(data)
    data match {
      case Data(Some(name), Some(phoneNumber), Some(city), Some(state), Some(zip), Some(address)) => Future(data) // original data is complete no need to return it here

      case Data(_, Some(phoneNumber), _, _, _, _) => // query phone service

        val query = Query.phone(phoneNumber)
        val futResult: Future[Either[ErrorResult, Result]] = request(query)
        futResult.map {
          case Left(error) =>
            throw new WhitepagesException(error.error.message)
          case Right(result) =>
            implicit val dictionary = Dictionary(result.dictionary)
            val phones: List[Phone] = result.results.flatMap(id => dictionary.phone(id))
            val results = Data.phonesToDatas(phones)
            selectBest(results, data)
        }

      case Data(_, _, city, state, Some(zip), Some(address)) =>
        // query location service, fill city, state and name
        val query = Query.location(address, Some(zip), city, state)
        val futResult: Future[Either[ErrorResult, Result]] = request(query)
        futResult.map {
          case Left(error) =>
            throw new WhitepagesException(error.error.message)
          case Right(result) =>
            implicit val dictionary = Dictionary(result.dictionary)
            val locations: List[Location] = result.results.flatMap(id => dictionary.location(id))
            val results = Data.locationsToDatas(locations)
            selectBest(results, data)
        }

      case Data(_, _, Some(city), state, zip, Some(address)) =>
        // query location service, fill city, zip and name
        val query = Query.location(address, zip, Some(city), state)

        val futResult: Future[Either[ErrorResult, Result]] = request(query)
        futResult.map {
          case Left(error) =>
            throw new WhitepagesException(error.error.message)
          case Right(result) =>
            implicit val dictionary = Dictionary(result.dictionary)
            val locations: List[Location] = result.results.flatMap(id => dictionary.location(id))
            val results = Data.locationsToDatas(locations)
            selectBest(results, data)
        }

      case Data(Some(name), _, None, None, None, None) =>
        // query only person service
        val futResult: Future[Either[ErrorResult, Result]] = request(Query.person(name))
        futResult.map {
          case Left(error) =>
            throw new WhitepagesException(error.error.message)
          case Right(result) =>
            implicit val dictionary = Dictionary(result.dictionary)
            val persons: List[Person] = result.results.flatMap(id => dictionary.person(id))
            val entities: List[Either[Person, Business]] = persons.map(Left(_))
            val results = Data.entitiesToDatas(entities)
            selectBest(results, data)
        }

      case Data(Some(name), _, city, state, zip, address) =>
        // query business and person service
        val personQuery = Query.person(name, city, state, zip, address)
        val businessQuery = Query.business(name, city, state, zip, address)

        val futPersonResult: Future[Either[ErrorResult, Result]] = request(personQuery)
        val futPersonDataResults: Future[List[DataResult]] = futPersonResult.map {
          case Left(error) =>
            throw new WhitepagesException(error.error.message)
          case Right(result) =>
            implicit val dictionary = Dictionary(result.dictionary)
            val persons: List[Person] = result.results.flatMap(id => dictionary.person(id))
            val entities: List[Either[Person, Business]] = persons.map(Left(_))
            Data.entitiesToDatas(entities)
        }

        val futBusinessResult: Future[Either[ErrorResult, Result]] = request(businessQuery)
        val futBusinessDataResults: Future[List[DataResult]] = futBusinessResult.map {
          case Left(error) =>
            throw new WhitepagesException(error.error.message)
          case Right(result) =>
            implicit val dictionary = Dictionary(result.dictionary)
            val persons: List[Business] = result.results.flatMap(id => dictionary.business(id))
            val entities: List[Either[Person, Business]] = persons.map(Right(_))
            Data.entitiesToDatas(entities)
        }

        val results: Future[List[DataResult]] = for {
          personDataResults <- futPersonDataResults
          futBusinessDataResults <- futBusinessDataResults
        } yield personDataResults ++ futBusinessDataResults

        results.map(selectBest(_, data))

      case _ => Future(data) // no data, can't do anything

    }
  }

  def mapper(in: Array[String]): Map[Int, Int] =
    in.zipWithIndex.foldLeft(Map[Int, Int]())((m, header) => m + (header match {
      case ("name", idx) => (0 -> idx)
      case ("phonenumber", idx) => (1 -> idx)
      case ("city", idx) => (2 -> idx)
      case ("state", idx) => (3 -> idx)
      case ("zip", idx) => (4 -> idx)
      case ("address", idx) => (5 -> idx)
      case _ => throw new IllegalStateException("Unknown header column: " + header._1)
    }))

  // curl -F f1=@phonetest.txt http://localhost:8080/file?api_key=7ef4d910d9419e55fa55ad9f29ef24ae

  /**
   * Input lines of the csv file -> output with possible fill outs.
   */
  def fixLines(lines: Stream[String], mapTo: Map[Int, Int])(implicit apiKey: ApiKey): Future[Stream[String]] = {
    val reverseMapTo = mapTo.map(_.swap) // reverse map to convert back to original order
    val originalDatas: Stream[Data] = lines.map(_.split("\\" + CSV_SEPARATOR, -1).map(_.trim)).map(line => Data(line, mapTo))
    val resultDatas: Stream[Future[Data]] = originalDatas.map(fix) // filling missing data
    Future.sequence(resultDatas).map(_.map(_.asString(reverseMapTo))) // transforming back original column order
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }

  val myRoute = {
    pathPrefix("download") {
      getFromDirectory("./download")
    } ~
      (path("") & get) { // heroku needs this to determine aliveness
        respondWithMediaType(`text/html`) {
          complete {
            <html>
              <body>
                <h1>Whitepages Batch service is running on heroku.</h1>
              </body>
            </html>
          }
        }
      } ~
      (path("file") & post & respondWithMediaType(`text/html`)) {
        entity(as[MultipartFormData]) { formData =>
          parameter('api_key) { key =>
            onComplete(Future {
              implicit val apiKey = ApiKey(key)
              val lines = formData.fields.map(_.entity.asString).flatten.foldLeft("")(_ + _).split("\n").toStream
              val headerLine = lines.head
              val headers = headerLine.split("\\" + CSV_SEPARATOR, -1).map(_.trim)

              val mapTo = mapper(headers.map(_.toLowerCase))
              val futLines: Future[Stream[String]] = fixLines(lines.tail, mapTo)

              futLines.map(_.foldLeft(headerLine)((text, line) => text + "\n" + line))
                .map { content =>
                  import java.io.File
                  val filename = java.util.UUID.randomUUID().toString()
                  val path = "." + File.separator + "download" + File.separator + filename
                  printToFile(new File(path)) { p =>
                    p.println(content)
                  }
                  filename
                }

            }.flatMap(identity)) {
              case Success(filename) =>
                redirect("/download/" + filename, StatusCodes.TemporaryRedirect)
              case Failure(ex) => ex match {
                case wpe: WhitepagesException =>
                  val msg = "Whitepages: " + wpe.getMessage
                  log.warning(msg)
                  complete(StatusCodes.InternalServerError, msg)
                case ise: IllegalStateException =>
                  log.warning(ise.getMessage, ise)
                  complete(StatusCodes.BadRequest, ise.getMessage)
                case aiobe: ArrayIndexOutOfBoundsException =>
                  log.error("malformed input file", aiobe)
                  complete(StatusCodes.BadRequest, "input file is malformed")
                case t: Throwable =>
                  log.error("internal error", t)
                  complete(StatusCodes.InternalServerError, "something went wrong")
              }
            }
          }
        }
      } ~
      (path("phone.json") & get) { //https://proapi.whitepages.com/2.0/phone.json?phone_number=2069735100&api_key=7ef4d910d9419e55fa55ad9f29ef24a
        respondWithMediaType(`application/json`) {
          complete {
            TestData.phone
          }
        }
      } ~
      (path("person.json") & get) { //https://proapi.whitepages.com/2.0/person.json?name=Jane+Smith&api_key=7ef4d910d9419e55fa55ad9f29ef24a
        respondWithMediaType(`application/json`) {
          complete {
            TestData.person
          }
        }
      } ~
      (path("business.json") & get) { //https://proapi.whitepages.com/2.0/business.json?name=toyota&city=Seattle&page_first=9&page_len=3&api_key=7ef4d910d9419e55fa55ad9f29ef24a
        respondWithMediaType(`application/json`) {
          complete {
            TestData.business
          }
        }
      } ~
      (path("location.json") & get) { //https://proapi.whitepages.com/2.0/location.json?street_line_1=403%20W%20Howe%20St&city=Seattle&state=WA&api_key=7ef4d910d9419e55fa55ad9f29ef24ae
        respondWithMediaType(`application/json`) {
          complete {
            TestData.location
          }
        }
      } ~
      pathPrefix("webapp") {
        getFromDirectory("./webapp")
      } ~ pathPrefix("bower_components") {
        getFromDirectory("./bower_components")
      } ~
      (path("wp") & get) {
        requestUri { uri =>
          respondWithHeader(RawHeader("Access-Control-Allow-Origin", "*")) {
            val query = "https://proapi.whitepages.com/2.0/identity_score.json?" + uri.query
            println(s"request: ${query}")
            val request = Get(query)
            complete(pipeline(request))
          }
        }
      }
  }

}
