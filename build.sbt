name := """wp-batch"""

packageArchetype.java_application

organization  := "com.whitepages"

version       := "0.1"

scalaVersion  := "2.11.2"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")


libraryDependencies ++= {
  val akkaV = "2.3.5"
  val sprayV = "1.3.1"
  Seq(
    "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-client"  % sprayV,
    "io.spray"            %%  "spray-json"    % sprayV,
    "io.spray"            %%  "spray-testkit" % sprayV    % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV     % "test",
    "org.specs2"          %%  "specs2-core"   % "2.3.11"  % "test",
    "org.scalatest"       %%  "scalatest"     % "2.2.1"   % "test"
  )
}

sublimeExternalSourceDirectoryParent <<= baseDirectory

sublimeTransitive := true

Revolver.settings

scalariformSettings

mainClass in Compile := Some("com.whitepages.batchservice.Boot")

mainClass in Revolver.reStart := Some("com.whitepages.batchservice.Boot")
